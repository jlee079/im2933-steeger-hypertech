/********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Library: LREAL_Tool
 * File: dtoa.c
 * Author: Isaac
 * Created: July 27, 2011
 *******************************************************************/

#include <bur/plctypes.h>
#include <AsMath.h>



	unsigned int	DoubleToA(double Input, char * pString){
		
		double	TempLREAL,	CurDigit,	Power;
		char	*pChar;
		unsigned short	DigitFound, i, j;
		

		pChar=		pString;
		TempLREAL=	Input;
		DigitFound=	0;
					
		if(TempLREAL < 0){
			*pChar=	45;
			pChar++;
			TempLREAL= (TempLREAL * -1);	
		}
		
		for(i=0; i<15; i++){
			
			/*Calculate this digit's power of 10*/
			Power=	1;
			for(j=0; j<(15-i); j++){
				Power=	Power*10;
			}
			
			/*Find the digit value*/
			CurDigit=		floor(TempLREAL / Power);
			if((CurDigit > 0) || DigitFound){
				DigitFound=	1;
				TempLREAL=	TempLREAL - (CurDigit * Power);
				*pChar=	(unsigned short int)CurDigit + 48;
				pChar++;
			}
		}
		
		/*The last digit isn't part of the loop*/
		CurDigit=	floor(TempLREAL);
		*pChar=	(unsigned short int)CurDigit + 48;
		pChar++;
		*pChar=	0;
		
		/*Return success*/
		return strlen(pString);

	}
	