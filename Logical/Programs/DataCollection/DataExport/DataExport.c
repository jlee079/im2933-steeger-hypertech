/********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Program: DataCollect
 * File: DataExport.c
 * Author: Isaac
 * Created: July 25, 2011
 ********************************************************************
 * Used to export the collected data. Also manages HMI display since
 * it is similar stuff.
 ********************************************************************/


/*==========================================
========	Defines and Includes	========
==========================================*/


	/*Includes*/
	#include <bur/plctypes.h>
	#ifdef _DEFAULT_INCLUDES
	 #include <AsDefault.h>
	#endif



	/*Macros*/
	
		/*Limit functions*/
		#define MAX(x,max)	(((x) >= (max)) ? (x) : (max))		/*Returns the larger of two values*/
		#define MIN(min,x)	(((x) <= (min)) ? (x) : (min))		/*Returns the smaller of two values*/
		#define LIMIT(min,x,max)	MIN(MAX(min,x),max)			/*Returns 'x' or limits 'x' to 'min' if 'x' is below min, limits to 'max' if 'x' is above 'max'*/
	
		/*RING: modulates x to be within the bounds of min and max and returns the value*/
		#define RING(min,x,max)		( (((x)-(min)) > ((max)-(min))) ? ((((x)-(min)) % ((max)-(min)+1)) + (min)) : ( (((x)-(min)) < 0) ? ((max)-(((min)-(x)-1) % ((max)-(min)+1))) : (x) ) )

		/*Defaulter*/
		#define DEF_VAL(num, value) num= (((num) == 0) ? (value) : (num))	/*Sets num to value if num was zero, otherwise, num is set to itself*/

		/*Range functions*/
		/*HYSTER: Sets 'status' to false when 'x' goes below 'low' and to true when 'x' goes above 'high' (low must be less than high, and function must be only thing on line)*/
		#define	HYSTER(status, low, x, high) status = ( ((x) >= (high) || status) && ((x) > (low)) )

		/*DEAD_BAND: Returns true if 'x' is >= 'low' and <= 'high'*/
		#define DEAD_BAND(low, x, high)	(((x) >= (low)) && ((x) <= (high)))

		/*Bit operator */
		#define BIT_SET(num, pos) num |= (1 << (pos))			/*Set bit 'pos' in the variable 'num' to true*/
		#define BIT_CLEAR(num, pos) num &= ~(1 << (pos))		/*Set bit 'pos' in the variable 'num' to false*/
		#define BIT_TOG(num, pos) num ^= (1 << (pos))			/*Toggles bit 'pos' in the variable 'num'*/
		#define BIT_CHECK(num, pos) (((num) >> (pos)) & 1 )		/*Returns the value of bit 'pos' in variable 'num', note that because this is a return, num can be an expression and so is in parenthesis*/
		#define BIT_EQ(num, pos, mybool) num = (num | ((UINT)(mybool) << (pos))) & ~((UINT)!(mybool) << (pos))		/*Sets bit 'pos' in variable 'num' to the value of 'mybool'*/
		#define BIT_NE(num, pos, mybool) num = (num | ((UINT)!(mybool) << (pos))) & ~((UINT)(mybool) << (pos))		/*Sets bit 'pos' in variable 'num' to the value of NOT 'mybool'*/

	   /*Misc Functions*/
		#define XOR(x, y)	(((x) || (y)) && !((x) && (y)))



	/*Defines*/
	#define	RESERVED_MEM	70000	/*inflated for testing, actual worst case estimate is 65500*/



/*==================================
========		INIT		========
==================================*/

void _INIT DataExportINIT( void )
{

	/*End of line and end of file chars*/
	CRLF[0]=	13;	/*CR*/
	CRLF[1]=	10;	/*LF*/
	CRLF[2]=	0;	/*Null Terminator*/
	
	/*Number of records to process per cycle*/
	ExpLinesPer=	10;
	
	/*Create event strings, limit 40 characters*/
	/*40char gauge:		  ========================================*/
	strcpy(ExpEvents[0], "No Record");
	strcpy(ExpEvents[1], "Pay Off Above Tension Limit");
	strcpy(ExpEvents[2], "Pay Off Below Tension Limit");
	strcpy(ExpEvents[3], "Take Up Above Tension Limit");
	strcpy(ExpEvents[4], "Take Up Below Tension Limit");
	strcpy(ExpEvents[5], "Strip Pay Off #1 Above Tension Limit");
	strcpy(ExpEvents[6], "Strip Pay Off #1 Below Tension Limit");
	strcpy(ExpEvents[7], "Strip Pay Off #2 Above Tension Limit");
	strcpy(ExpEvents[8], "Strip Pay Off #2 Below Tension Limit");
	strcpy(ExpEvents[9], "Pay Off Traverse Centerline Error");
	strcpy(ExpEvents[10], "Take Up Traverse Centerline Error");
	
	
	/*Misc*/
	SuccessTimer.PT=	4000;
	FailTimer.PT=		4000;
	HMI_ConfReset=		1;
	
	
	/*Allocate Memory for the export*/
	AllocStatus = TMP_alloc(RESERVED_MEM, &pAllocMem);
	
	
}/*End INIT*/



/*==================================
========		CYCLIC		========
==================================*/

void _CYCLIC DataExportCYCLIC( void )
{
	
	/*=========	Catch Commands	=========*/
		
		/*Export Command*/
		if(ExpCmdExport){
			ExpCmdExport=	0;
			if(EXPORT_STATE == 0){
				EXPORT_STATE=	1;
			}
		}
		
		/*Reset Command*/
		if(ExpCmdReset){
			ExpCmdReset=	0;
			memset(&ErrorRecord, 0, sizeof(ErrorRecord));	
		}
	
	
	
	/*=========	Export State Machine ======*/
		
		switch(EXPORT_STATE){
	
			case	0:	/*Wait. Mostly reset function blocks*/
		
				GetTime.enable=			0;
				CreateFile.enable=		0;
				WriteFile.enable=		0;
				CloseFile.enable=		0;
				SuccessTimer.IN=		0;
				FailTimer.IN=			0;

			break;
		
		
			case	1:	/*Create file name*/
		
				/*Get the time in seconds*/
				GetTime.enable=	1;
				if(GetTime.status < 65534){
					if(GetTime.status == 0){
						GetTime.enable=	0;
						ExpTempTime=	GetTime.DT1;
						strcpy(ExpFileName, "FaultRecord_");
						itoa((DINT)ExpTempTime, ExpTempString);
						strcat(ExpFileName, ExpTempString);
						strcat(ExpFileName, ".csv");
						EXPORT_STATE=	2;
					}else{
						ExpFubStatus=	GetTime.status;
						FailTimer.IN=	1;
						EXPORT_STATE=	0;
					}
				}

			break;
		
		
			case	2:	/*Create file*/
		
				CreateFile.enable=	1;
				if(CreateFile.status < 65534){
				
					CreateFile.enable=	0;
					if(CreateFile.status == 0){
						ExpFileIdent=	CreateFile.ident;
						EXPORT_STATE=	3;
					}else{
						ExpFubStatus=	CreateFile.status;
						FailTimer.IN=	1;
						EXPORT_STATE=	6;	
					}
			
				}
		
			break;
		
		
			case	3:	/*Prep File*/

				/*Set some line tracking variables*/
				ExpLineStart=	RING(0,	(ErrorRecord.NextEntry - 1), 499);
				ExpLineNum=		ExpLineStart;
		
				/*Clear the memory arrea*/
				memset(pAllocMem, 0, RESERVED_MEM);
				
				/*Write the Intro line or lines*/
				ascDT(ExpTempTime, ExpTempString, 25);
				strcpy(ExpWriteLine, "Error Record V1.0, exported: ");
				strcat(ExpWriteLine, ExpTempString);
				strcpy(pAllocMem, ExpWriteLine);
				ExpOffset=	strlen(ExpWriteLine);
				strcpy(pAllocMem + ExpOffset, &CRLF);
				ExpOffset=	ExpOffset + 1;
			
				/*Head the columns*/
				strcpy(ExpWriteLine, "DATE,EVENT,START POSITION,END POSITION,LIMIT,PEAK VALUE");
				strcpy(pAllocMem + ExpOffset, ExpWriteLine);
				ExpOffset=	ExpOffset + strlen(ExpWriteLine);
				strcpy(pAllocMem + ExpOffset, &CRLF);
				ExpOffset=	ExpOffset + 1;
			
				/*Go to line writing*/
				EXPORT_STATE=	4;
			
			break;
		
		
			case	4:	/*Construct Entries*/
		
				/*This loops through a few lines per cycle starting at the latest and work back to the oldest*/
				for(i=0; i<ExpLinesPer; i++){
			
					/*Build entry*/

						/*Date*/
						itoa(ErrorRecord.Record[RING(0, (ExpLineNum - i), 499)].FaultTime.month, ExpTempString);
						strcpy(ExpWriteLine, ExpTempString);
						strcat(ExpWriteLine, "/");
						itoa(ErrorRecord.Record[RING(0, (ExpLineNum - i), 499)].FaultTime.day, ExpTempString);
						strcat(ExpWriteLine, ExpTempString);
						strcat(ExpWriteLine, "/");
						itoa(ErrorRecord.Record[RING(0, (ExpLineNum - i), 499)].FaultTime.year, ExpTempString);
						strcat(ExpWriteLine, ExpTempString);
						strcat(ExpWriteLine, " - ");
						itoa(ErrorRecord.Record[RING(0, (ExpLineNum - i), 499)].FaultTime.hour, ExpTempString);
						strcat(ExpWriteLine, ExpTempString);
						strcat(ExpWriteLine, ":");
						itoa(ErrorRecord.Record[RING(0, (ExpLineNum - i), 499)].FaultTime.minute, ExpTempString);
						strcat(ExpWriteLine, ExpTempString);
						strcat(ExpWriteLine, ":");
						itoa(ErrorRecord.Record[RING(0, (ExpLineNum - i), 499)].FaultTime.second, ExpTempString);
						strcat(ExpWriteLine, ExpTempString);
						strcat(ExpWriteLine, ",");
					
						/*Event*/
						strcat(ExpWriteLine, ExpEvents[ErrorRecord.Record[RING(0, (ExpLineNum - i), 499)].FaultType]);
						strcat(ExpWriteLine, ",");
					
						/*StartPos*/
						DoubleToA(ErrorRecord.Record[RING(0, (ExpLineNum - i), 499)].FaultPos, ExpTempString);
						strcat(ExpWriteLine, ExpTempString);
						strcat(ExpWriteLine, ",");
					
						/*EndPos*/
						DoubleToA(ErrorRecord.Record[RING(0, (ExpLineNum - i), 499)].FaultEndPos, ExpTempString);
						strcat(ExpWriteLine, ExpTempString);
						strcat(ExpWriteLine, ",");
					
						/*EndPos*/
						ftoa(ErrorRecord.Record[RING(0, (ExpLineNum - i), 499)].LimitVal, ExpTempString);
						strcat(ExpWriteLine, ExpTempString);
						strcat(ExpWriteLine, ",");
					
						/*PeakValue*/
						ftoa(ErrorRecord.Record[RING(0, (ExpLineNum - i), 499)].FaultVal, ExpTempString);
						strcat(ExpWriteLine, ExpTempString);
					
						/*End Line*/
						strcpy(pAllocMem + ExpOffset, ExpWriteLine);
						ExpOffset=	ExpOffset + strlen(ExpWriteLine);
						strcpy(pAllocMem + ExpOffset, &CRLF);
						ExpOffset=	ExpOffset + 1;

							
					/*Check if that was the last record*/
					if(RING(0, (ExpLineNum - i), 499) == RING(0, (ExpLineStart + 1), 499)){
						EXPORT_STATE=	5;
						break;
					}
					
				}
			
				/*Prep for next run*/
				ExpLineNum=		RING(0, (ExpLineNum - ExpLinesPer), 499);
		
			break;
		
		
			case	5:	/*Write the file*/
		
				WriteFile.pSrc=		pAllocMem;
				WriteFile.len=		RESERVED_MEM;	/*It will stop writing at the first null terminator*/
				WriteFile.enable=	1;
				if(WriteFile.status < 65534){
				
					WriteFile.enable=	0;
					if(WriteFile.status == 0){
						SuccessTimer.IN=	1;
						EXPORT_STATE=		6;
					}else{
						ExpFubStatus=		WriteFile.status;
						FailTimer.IN=		1;
						EXPORT_STATE=		6;	
					}
			
				}
		
			break;
		
		
			case	6:	/*Close the file*/
		
				CloseFile.enable=	1;
				if(CloseFile.status < 65534){
					CloseFile.enable=	0;
				
					if(CloseFile.status == 0){
						EXPORT_STATE=	0;
					}else{
						ExpFubStatus=	CloseFile.status;
						EXPORT_STATE=	0;	
					}
			
				}
		
			break;
		
		}/*End Export state machine*/
		
		
		
	/*========	HMI Display	=========*/
	
		/*Keep the selector within allowed values, this just prevents weird behavior at the UINT overflow*/
		HMI_ErrorRecord.Selector=	RING(0, HMI_ErrorRecord.Selector, 499);
		
		/*Update the display fields and record numbers*/
		for(i=0; i<5; i++){
			
			/*I had to do this field by field, memcpy was mysteriously causing a memory IO error*/
			HMI_ErrorRecord.Record[i].FaultEndPos=		ErrorRecord.Record[RING(0, (ErrorRecord.NextEntry - i - HMI_ErrorRecord.Selector), 499)].FaultEndPos;
			HMI_ErrorRecord.Record[i].FaultPos=			ErrorRecord.Record[RING(0, (ErrorRecord.NextEntry - i - HMI_ErrorRecord.Selector), 499)].FaultPos;
			HMI_ErrorRecord.Record[i].FaultType=		ErrorRecord.Record[RING(0, (ErrorRecord.NextEntry - i - HMI_ErrorRecord.Selector), 499)].FaultType;
			HMI_ErrorRecord.Record[i].FaultVal=			ErrorRecord.Record[RING(0, (ErrorRecord.NextEntry - i - HMI_ErrorRecord.Selector), 499)].FaultVal;
			HMI_ErrorRecord.Record[i].LimitVal=			ErrorRecord.Record[RING(0, (ErrorRecord.NextEntry - i - HMI_ErrorRecord.Selector), 499)].LimitVal;
			HMI_ErrorRecord.Record[i].FaultTime.day=	ErrorRecord.Record[RING(0, (ErrorRecord.NextEntry - i - HMI_ErrorRecord.Selector), 499)].FaultTime.day;
			HMI_ErrorRecord.Record[i].FaultTime.month=	ErrorRecord.Record[RING(0, (ErrorRecord.NextEntry - i - HMI_ErrorRecord.Selector), 499)].FaultTime.month;
			HMI_ErrorRecord.Record[i].FaultTime.year=	ErrorRecord.Record[RING(0, (ErrorRecord.NextEntry - i - HMI_ErrorRecord.Selector), 499)].FaultTime.year;
			HMI_ErrorRecord.Record[i].FaultTime.hour=	ErrorRecord.Record[RING(0, (ErrorRecord.NextEntry - i - HMI_ErrorRecord.Selector), 499)].FaultTime.hour;
			HMI_ErrorRecord.Record[i].FaultTime.minute=	ErrorRecord.Record[RING(0, (ErrorRecord.NextEntry - i - HMI_ErrorRecord.Selector), 499)].FaultTime.minute;
			HMI_ErrorRecord.Record[i].FaultTime.second=	ErrorRecord.Record[RING(0, (ErrorRecord.NextEntry - i - HMI_ErrorRecord.Selector), 499)].FaultTime.second;

			HMI_ErrorRecord.HMInums[i]=	RING(1, (i + HMI_ErrorRecord.Selector + 1), 500);
			strcpy(HMI_ErrorRecord.HMIevents[i], ExpEvents[HMI_ErrorRecord.Record[i].FaultType]);
			
		}
		
		/*Visibility Control*/
		BIT_NE(HMI_ExpSuccess,	0,	SuccessTimer.Q);
		BIT_NE(HMI_ExpFail,		0,	FailTimer.Q);
		
		/*Progress Bar*/
		if(EXPORT_STATE == 0){
			HMI_ExpProg=	0;	
		}else{
			HMI_ExpProg=	RING(0, (ExpLineStart - ExpLineNum), 499);
		}
	
	
	
	/*========	Call Fubs	=========*/
	
		CreateFile.pDevice=	&("FPrompt");	/*There is no access to USB, so I export to F:\ now*/
		CreateFile.pFile=	&ExpFileName;
		FileCreate(&CreateFile);
	
		WriteFile.ident=	ExpFileIdent;
		WriteFile.offset=	0;
		FileWrite(&WriteFile);
	
		CloseFile.ident=	ExpFileIdent;
		FileClose(&CloseFile);
	
		DTGetTime(&GetTime);
		
		TOF(&SuccessTimer);
		TOF(&FailTimer);
	
	
	
	/*=========	State Trace	=========*/

	if(EXPORT_STATE != StateTrace.EXPORT_STATE.State[StateTrace.EXPORT_STATE.Last]){
		StateTrace.EXPORT_STATE.Last++;
		StateTrace.EXPORT_STATE.Last=	RING(0, StateTrace.EXPORT_STATE.Last, 20);
		StateTrace.EXPORT_STATE.State[StateTrace.EXPORT_STATE.Last]=	EXPORT_STATE;
		StateTrace.EXPORT_STATE.Status[StateTrace.EXPORT_STATE.Last]=	ExpFubStatus;	
	}

	
	
}/*End CYCLIC*/
