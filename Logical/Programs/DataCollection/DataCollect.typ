(********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Package: DataCollection
 * File: DataCollect.typ
 * Author: Isaac
 * Created: July 26, 2011
 ********************************************************************
 * Data types of package DataCollection
 ********************************************************************)

TYPE

	FaultRecord_typ			:	STRUCT		(*All Rem collection data*)
		
		NextEntry	: UINT;		(*The record to be recorded*)
		Record		: ARRAY[0..499] OF FaultRecord_Record_typ;		(*Record of what, when and by how much*)
		
	END_STRUCT;
	
	FaultRecord_Record_typ	:	STRUCT		(*Single entry in data Logger*)
		
		FaultPos	: LREAL;		(*Position of Puller*)
		FaultEndPos	: LREAL;		(*Position when Fault condition ended*)
		FaultTime	: DTStructure;	(*Time of error condition start*)
		FaultType	: USINT;		(*Which axis and over or under*)
		LimitVal	: REAL;			(*From Recipe*)
		FaultVal	: REAL;			(*Peak value of Load cell in direction of fault or centerline offset value*)
		
	END_STRUCT;
	
	
	FaultRecord_HMI_typ		:	STRUCT		(*Used to create display for HMI*)
		
		Selector	: UINT;		(*What record to display. 0 shows newest*)
		Record		: ARRAY[0..4] OF FaultRecord_Record_typ;	(*display records*)
		HMInums		: ARRAY[0..4] OF UINT;			(*Indicators for what record it is*)
		HMIevents	: ARRAY[0..4] OF STRING[40];	(*Recording the event string simplifies display*)
		
	END_STRUCT;
	
	
	Export_Local_typ	:	STRUCT	(*Organizing Eport variables*)
		
		LastEntry	: UINT;		(*The last recorded record*)
		Record		: ARRAY[0..499] OF FaultRecord_Record_typ;		(*Record of what, when and by how much*)
		
	END_STRUCT;	
	
	
	StateTrace_typ			:	STRUCT	(*Trace state machines*)
		
		EXPORT_STATE	: StateTrace_State_typ;
		
	END_STRUCT;
	
	StateTrace_State_typ	:	STRUCT	(*Record for state machine trace*)
		
		Last		: USINT;
		State		: ARRAY[0..20] OF USINT;
		Status		: ARRAY[0..20] OF UINT;
		
	END_STRUCT;

END_TYPE
