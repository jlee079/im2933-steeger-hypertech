
TYPE
	LoadCell_typ : 	STRUCT 
		Value_raw : DINT;
		Value_eff : REAL;
		Value_eff_last : REAL;
		Value_eff_filtered : REAL;
		Value_eff_v : REAL;
		Force : REAL;
		Force_filtered : REAL;
		ModuleStatus : USINT;
		ADCConfig : USINT;
		ValueEffFilter : MovingAverageN;
		ValueEffSpeedFilter : MovingAverageN;
	END_STRUCT;
	Encoder_typ : 	STRUCT 
		Value : DINT;
		Value_last : DINT;
		TimeChanged : DINT;
		TimeValid : DINT;
		TimeValid_last : DINT;
		Zero : BOOL;
		WireBreakChA : BOOL;
		WireBreakChB : BOOL;
		WireBreakReset : BOOL;
		Velocity : REAL;
		VelocityFilter : MovingAverageN;
	END_STRUCT;
	CenterLine_typ : 	STRUCT 
		Value_eff : INT;
		Displacement : REAL;
	END_STRUCT;
	FootPedal_typ : 	STRUCT 
		Enabled : BOOL;
		Value_raw : INT;
		Step : REAL;
	END_STRUCT;
	IOForce_typ : 	STRUCT 
		cmd : IOForce_cmd_typ;
		data : IOForce_data_typ;
		Channel : ARRAY[0..103] OF IOForce_Channel_typ;
	END_STRUCT;
	IOForce_cmd_typ : 	STRUCT 
		DisableAllForcing : BOOL;
	END_STRUCT;
	IOForce_data_typ : 	STRUCT 
		ForcingEnabled : BOOL;
	END_STRUCT;
	IOForce_Channel_typ : 	STRUCT 
		Enable : BOOL;
		ForceValue : DINT;
		PhysicalValue : DINT;
		Step : UINT;
		Datapoint : STRING[50];
		FB : IOForce_Channel_FB_typ;
	END_STRUCT;
	IOForce_Channel_FB_typ : 	STRUCT 
		EnableForcing : AsIOEnableForcing;
		DisableForcing : AsIODisableForcing;
		SetForceValue : AsIOSetForceValue;
		DatapointStatus : AsIODPStatus;
	END_STRUCT;
	Stats_typ : 	STRUCT 
		OffError : UDINT;
		On : UDINT;
		Ready : UDINT;
		Running : UDINT;
		TableRevs : LREAL;
		PullerDistance : LREAL;
	END_STRUCT;
	StatsVC_typ : 	STRUCT 
		Day : UINT;
		Hour : UINT;
		Minute : UINT;
		Second : UINT;
	END_STRUCT;
	MMBraider_typ : 	STRUCT 
		Mode : UINT;
		SetMode : UINT;
		cmd : MMBraider_cmd_typ;
		data : MMBraider_data_typ;
	END_STRUCT;
	MMBraider_data_typ : 	STRUCT 
		WireBreak : BOOL;
		ErrorID : UINT;
		AutoGeared : BOOL;
		TableGeared : BOOL;
		AutoTuneStatus : UINT;
		JogPosTableStatus : UINT;
		JogNegTableStatus : UINT;
		MachineState : UINT;
		PowerLoss : BOOL;
		AutoMsg : UINT;
	END_STRUCT;
	MMBraider_cmd_typ : 	STRUCT 
		AutoGearIn : BOOL;
		ErrorAcknowledge : BOOL;
		AutoTuneTable : BOOL;
		JogPosTablePB : BOOL;
		JogNegTablePB : BOOL;
		ManualGearIn : BOOL;
		ManualGearInLoading : USINT;
	END_STRUCT;
	MMPuller_typ : 	STRUCT 
		Mode : UINT;
		SetMode : UINT;
		cmd : MMPuller_cmd_typ;
		data : MMPuller_data_typ;
	END_STRUCT;
	MMPuller_data_typ : 	STRUCT 
		WireBreak : BOOL;
		ErrorID : UINT;
		PullerGeared : BOOL;
		AutoTuneStatus : UINT;
		JogPosPullerStatus : UINT;
		JogNegPullerStatus : UINT;
		MachineState : UINT;
		PowerLoss : BOOL;
		AutoMsg : UINT;
	END_STRUCT;
	MMPuller_cmd_typ : 	STRUCT 
		ErrorAcknowledge : BOOL;
		AutoTunePuller : BOOL;
		JogPosPullerPB : BOOL;
		JogNegPullerPB : BOOL;
		ManualGearIn : BOOL;
		ManualGearPPI : REAL;
		ManualGearInLoading : USINT;
	END_STRUCT;
	MMPayOff_typ : 	STRUCT 
		Mode : UINT;
		SetMode : UINT;
		cmd : MMPayOff_cmd_typ;
		data : MMPayOff_data_typ;
	END_STRUCT;
	MMPayOff_data_typ : 	STRUCT 
		WireBreak : BOOL;
		ErrorID : UINT;
		AutoTuneStatus : UINT;
		JogPosSpoolStatus : UINT;
		JogNegSpoolStatus : UINT;
		JogPosTraverseStatus : UINT;
		JogNegTraverseStatus : UINT;
		MachineState : UINT;
		PowerLoss : BOOL;
		AutoMsg : UINT;
	END_STRUCT;
	MMPayOff_cmd_typ : 	STRUCT 
		ErrorAcknowledge : BOOL;
		AutoTuneSpool : BOOL;
		AutoTuneTraverse : BOOL;
		JogPosSpoolPB : BOOL;
		JogNegSpoolPB : BOOL;
		JogPosTraversePB : BOOL;
		JogNegTraversePB : BOOL;
	END_STRUCT;
	MMTakeUp_typ : 	STRUCT 
		Mode : UINT;
		SetMode : UINT;
		cmd : MMTakeUp_cmd_typ;
		data : MMTakeUp_data_typ;
	END_STRUCT;
	MMTakeUp_data_typ : 	STRUCT 
		WireBreak : BOOL;
		ErrorID : UINT;
		AutoTuneStatus : UINT;
		JogPosSpoolStatus : UINT;
		JogNegSpoolStatus : UINT;
		JogPosTraverseStatus : UINT;
		JogNegTraverseStatus : UINT;
		MachineState : UINT;
		PowerLoss : BOOL;
		AutoMsg : UINT;
	END_STRUCT;
	MMTakeUp_cmd_typ : 	STRUCT 
		ErrorAcknowledge : BOOL;
		AutoTuneSpool : BOOL;
		AutoTuneTraverse : BOOL;
		JogPosSpoolPB : BOOL;
		JogNegSpoolPB : BOOL;
		JogPosTraversePB : BOOL;
		JogNegTraversePB : BOOL;
	END_STRUCT;
	MMStripPayOff_typ : 	STRUCT 
		Mode : UINT;
		SetMode : UINT;
		cmd : MMStripPayOff_cmd_typ;
		data : MMStripPayOff_data_typ;
	END_STRUCT;
	MMStripPayOff_data_typ : 	STRUCT 
		WireBreak : BOOL;
		ErrorID : UINT;
		AutoTuneStatus : UINT;
		JogPosSpoolStatus : UINT;
		JogNegSpoolStatus : UINT;
		MachineState : UINT;
		PowerLoss : BOOL;
		AutoMsg : UINT;
	END_STRUCT;
	MMStripPayOff_cmd_typ : 	STRUCT 
		ErrorAcknowledge : BOOL;
		AutoTuneSpool : BOOL;
		JogPosSpoolPB : BOOL;
		JogNegSpoolPB : BOOL;
	END_STRUCT;
	AlarmMgr_typ : 	STRUCT 
		cmd : UINT;
		Init : BOOL;
		Banner : ARRAY[0..199] OF UINT;
		State : UINT;
		MessageState : UINT;
		PowerOnCycles : UDINT;
		OperatingHoursPP : UDINT;
		BatteryStatusCPU : USINT;
		TemperatureCPU : UINT;
		TemperatureENV : UINT;
		EStopOK : BOOL;
		ErrCritical : BOOL;
		ErrMMHandled : BOOL;
		ErrFatal : BOOL;
		AlarmBits : ARRAY[0..255] OF BOOL;
	END_STRUCT;
	Axis_Cmd_typ : 	STRUCT 
		On : BOOL;
		Home : BOOL;
		MoveVelocity : BOOL;
		MoveAbsolute : BOOL;
		MoveAdditive : BOOL;
		GearIn : BOOL;
		Distance : REAL;
		Position : REAL;
		Velocity : REAL;
		Acceleration : REAL;
		Deceleration : REAL;
		Direction : USINT;
		MasterIndex : UINT;
		MasterScale : REAL;
		SlaveScale : REAL;
		AutoTune : BOOL;
		AutomatInitParameters : BOOL;
		AutomatUpdateParameters : BOOL;
		AutomatEnable : BOOL;
		AutomatStart : BOOL;
		AutomatStop : BOOL;
		AutomatRestart : BOOL;
		AutomatEnd : BOOL;
		AutomatSignal1 : BOOL;
		AutomatSignal2 : BOOL;
		AutomatSignal3 : BOOL;
		AutomatSignal4 : BOOL;
		Automat : MC_AUTDATA_TYP;
		AutomatReadRestartPosition : BOOL;
		DownloadCamProfileData : BOOL;
		DownloadCamProfileObj : BOOL;
		DownloadCamProfileName : STRING[12];
		DownloadCamProfileIndex : UINT;
		CamProfileData : MC_CAMPROFILE_TYP;
		ErrorAcknowledge : BOOL;
		SetEncoderDirection : BOOL;
		EncoderDirection : UINT;
		TorqueControl : BOOL;
		Torque : REAL;
		VelocityControl : BOOL;
		CyclicVelocity : REAL;
		CyclicVelocityCorrection : REAL;
		CyclicTorque : REAL;
		UnitFactor : UDINT;
		AxisPeriod : UDINT;
	END_STRUCT;
	Axis_Data_typ : 	STRUCT 
		Error : BOOL;
		ErrorID : UDINT;
		ErrorText : ARRAY[0..3] OF STRING[79];
		Position : REAL;
		Positionraw : DINT;
		LastPosition : REAL;
		Velocity : REAL;
		AutomatActive : BOOL;
		AutomatRunning : BOOL;
		AutomatStandby : BOOL;
		AutomatActualState : USINT;
		AutomatRestartPosition : REAL;
		VelocityControlActive : BOOL;
		Iraw : REAL;
		Ic : REAL;
		Ip : REAL;
		FEraw : REAL;
		FE : REAL;
		MotorModelTemp : REAL;
		MotorSensorTemp : REAL;
		JunctionModelTemp : REAL;
		BleederTemp : REAL;
		DCBusVoltage : REAL;
		AutoTuneStatus : UINT;
		Homed : BOOL;
		Homing : BOOL;
		HomingError : BOOL;
		Encoder_pos_act : DINT;
		Encoder_pos : DINT;
		Encoder_v : REAL;
		aut_state_index : USINT;
		On : BOOL;
		dig_in_enable : USINT;
		dig_in_neg_hw_end : USINT;
		dig_in_pos_hw_end : USINT;
		dig_in_reference : USINT;
		dig_in_trigger1 : USINT;
		dig_in_trigger2 : USINT;
		ScaledAxisPeriod : UDINT;
	END_STRUCT;
	Axis_typ : 	STRUCT 
		Init : BOOL;
		Status : USINT;
		cmd : Axis_Cmd_typ;
		data : Axis_Data_typ;
	END_STRUCT;
	DataMgr_typ : 	STRUCT 
		Init : BOOL;
		cmd : UINT;
		NRec : UINT;
		NRecDst : UINT;
		Name : STRING[20];
		EmptySys : BOOL;
		ErrorID : UINT;
	END_STRUCT;
	HMI_Runtime_typ : 	STRUCT 
		Change : UINT;
		Current : UINT;
	END_STRUCT;
	HMI2_typ : 	STRUCT 
		ConnectionLost_SDP : UINT;
		RunStatus_ToggleLineDirPB : BOOL;
		RunStatus_ToggleFootPedalPB : BOOL;
		RunStatus_LineStoppedSDP : UINT;
		RunStatus1NextPB : BOOL;
		RunStatus3PrevPB : BOOL;
		Page : HMI_Runtime_typ;
		Language : HMI_Runtime_typ;
		KeyLevel : HMI_Runtime_typ;
		LinearDistanceUnits : HMI_Runtime_typ;
		LinearVelocityUnits : HMI_Runtime_typ;
		LinearAccelerationUnits : HMI_Runtime_typ;
		PitchUnits : HMI_Runtime_typ;
		TorqueUnits : HMI_Runtime_typ;
		SpeedTorqueUnits : HMI_Runtime_typ;
		InertiaUnits : HMI_Runtime_typ;
		TemperatureUnits : HMI_Runtime_typ;
		TouchStatus : USINT;
	END_STRUCT;
	HMI_typ : 	STRUCT 
		InitStatus : UINT;
		Continue_SDP : UINT;
		ContinuePB : BOOL;
		AbortJobPB : BOOL;
		ResetPB : BOOL;
		NotSaved_SDP : UINT;
		AutoBanner_SDP : UINT;
		StatsReset_SDP : UINT;
		InitJob : BOOL;
		InitJob_SDP : UINT;
		BraiderSetup_SDP : UINT;
		DateTime : RTCtime_typ;
		DateTimeLoad : BOOL;
		DateTimeSet : BOOL;
		AxisIndex : UINT;
		AxisListBraider_SDP : UINT;
		AxisListSolder_SDP : UINT;
		EncoderLoadCellIndex : UINT;
		EncoderLoadCellListBraider_SDP : UINT;
		EncoderLoadCellListSolder_SDP : UINT;
		CenterLineSensorIndex : UINT;
		CenterLineSensorList_SDP : UINT;
		AxisStatusLinear_SDP : UINT;
		AxisStatusRotary_SDP : UINT;
		AxisConfigLinear_SDP : UINT;
		AxisConfigRotary_SDP : UINT;
		AxisConfigLinearSteegerOnly_SDP : UINT;
		AxisConfigRotarySteegerOnly_SDP : UINT;
		AxisConfigLinearEndLimits_SDP : UINT;
		AutoTuneStartPB : BOOL;
		AutoTuneStatus : UINT;
		LoadCellCalibratePB : BOOL;
		LoadCellCalibrateCancelPB : BOOL;
		LoadCellCalibrateNextPB : BOOL;
		LoadCellCalibrationStatus : UINT;
		LoadCellTareForce : REAL;
		LoadCellTareForce_SDP : UINT;
		LoadCellLimitForce_SDP : UINT;
		LoadCellCalibrateStart_SDP : UINT;
		LoadCellCalibrateRunning_SDP : UINT;
		LoadCellCalibrateRetry_SDP : UINT;
		LoadCellCalibrateFinish_SDP : UINT;
		CenterLineCalibrateCancelPB : BOOL;
		CenterLineCalibrateNextPB : BOOL;
		CenterLineCalibrateStart_SDP : UINT;
		CenterLineCalibrateRunning_SDP : UINT;
		CenterLineCalibrateFinish_SDP : UINT;
		CenterLineCalibrateRange_SDP : UINT;
		CenterLineCalibrateRange : REAL;
		CenterLineCalibrateStatus : UINT;
		FootPedalCalibratePB : BOOL;
		FootPedalCalibrateCancelPB : BOOL;
		FootPedalCalibrateNextPB : BOOL;
		FootPedalCalibrateStart_SDP : UINT;
		FootPedalCalibrateRunning_SDP : UINT;
		FootPedalCalibrateFinish_SDP : UINT;
		FootPedalCalibrateStatus : UINT;
		EthernetSettingsLoad : BOOL;
		EthernetSettingsApply : BOOL;
		HMI2IPAddress : ARRAY[0..3] OF USINT;
		EthernetIPAddress : ARRAY[0..3] OF USINT;
		EthernetSubnetMask : ARRAY[0..3] OF USINT;
		EthernetGatewayAddress : ARRAY[0..3] OF USINT;
		EthernetINANode : USINT;
		EthernetSettingsStatus : USINT;
		RunMachineBraider_SDP : UINT;
		RunMachineSolder_SDP : UINT;
		RunBraiderEnabled_SDP : UINT;
		RunBraiderDisabled_SDP : UINT;
		RunPOSEnabled_SDP : UINT;
		RunPOTEnable_SDP : UINT;
		RunPOTEnabled_SDP : UINT;
		RunPOTModePitch_SDP : UINT;
		RunPOTModeCorrective_SDP : UINT;
		RunTUSModeInline_SDP : UINT;
		RunTUSModeStandalone_SDP : UINT;
		RunTUTEnable_SDP : UINT;
		RunTUTEnabled_SDP : UINT;
		RunTUTModePitch_SDP : UINT;
		RunTUTModeCorrective_SDP : UINT;
		RunStripPOS1Enabled_SDP : UINT;
		RunStripPOS2Enabled_SDP : UINT;
		RunModeDiscrete_SDP : UINT;
		RunTogglePOTModePB : BOOL;
		RunTogglePOTDirectionPB : BOOL;
		RunToggleTUSModePB : BOOL;
		RunToggleTUTModePB : BOOL;
		RunToggleTUTDirectionPB : BOOL;
		RunToggleSolderControlPB : BOOL;
		RunStatus_ToggleLineDirPB : BOOL;
		RecipeLoad_SDP : UINT;
		RecipeSave_SDP : UINT;
		RecipeImport_SDP : UINT;
		RecipeDelete_SDP : UINT;
		RecipeLoadPB : BOOL;
		RecipeSavePB : BOOL;
		RecipeDeletePB : BOOL;
		RecipeImportAllPB : BOOL;
		RecipeExportAllPB : BOOL;
		RecipeDeleteAllPB : BOOL;
		RecipeRenamePB : BOOL;
		RecipeImportPB : BOOL;
		RecipeExportPB : BOOL;
		RecipeStatus : UINT;
		RecipeName : STRING[20];
		RecipeN : UINT;
		RecipeN_ext : UINT;
		RecipeLoadPageLoad : BOOL;
		RecipeSavePageLoad : BOOL;
		RunStatusComplete_SDP : UINT;
		RunStatusContinuous_SDP : UINT;
		RunStatusDiscrete_SDP : UINT;
		RunStatusLength_SDP : UINT;
		RunStatusTableSpeed_SDP : UINT;
		RunStatusPullerSpeed_SDP : UINT;
		RunStatusBraider_SDP : UINT;
		RunStatusSolder_SDP : UINT;
		RunStatusPPI_SDP : UINT;
		RunStatusStopReset_SDP : UINT;
		RunStatusSave_SDP : UINT;
		RunStatus_LineStoppedSDP : UINT;
		AdvMaintFunctions_SDP : UINT;
		AdvAdminFunctions_SDP : UINT;
		RunStatus_SDP : UINT;
		JobInProgress_SDP : UINT;
		ContinueJobPB : BOOL;
		AdvIOSolder_SDP : UINT;
		AdvIOBraider_SDP : UINT;
		AdvMachineSetup_SDP : UINT;
		AdvMachineSetupBraider_SDP : UINT;
		AdvRecipeSetup_SDP : UINT;
		OEMToggleAxisDirPB : BOOL;
		OEMToggleAxisEncodValidPB : BOOL;
		OEMToggleAxisTempModelPB : BOOL;
		OEMToggleEncodDirPB : BOOL;
		OEMTogglePhaseIgnorePB : BOOL;
		OEMImportPB : BOOL;
		OEMExportPB : BOOL;
		OEMRestoreDefaultsPB : BOOL;
		OEMSysSettingsStatus : UINT;
		TrendBraiderValueScale_SDP : UINT;
		TrendSolderValueScale_SDP : UINT;
		Page : HMI_Runtime_typ;
		Language : HMI_Runtime_typ;
		KeyLevel : HMI_Runtime_typ;
		LinearDistanceUnits : HMI_Runtime_typ;
		LinearVelocityUnits : HMI_Runtime_typ;
		LinearAccelerationUnits : HMI_Runtime_typ;
		PitchUnits : HMI_Runtime_typ;
		TorqueUnits : HMI_Runtime_typ;
		SpeedTorqueUnits : HMI_Runtime_typ;
		InertiaUnits : HMI_Runtime_typ;
		TemperatureUnits : HMI_Runtime_typ;
		LoginPB : BOOL;
		LogoutPB : BOOL;
		LoginUser : USINT; (*5 = steeger, 4 = Administrator, 3 = Engineer. 2 = Maintenance 1 = Operator*)
		LoginLevel : USINT;
		LoginStatus : USINT;
		LoginPassword : STRING[6];
		TouchStatus : USINT;
	END_STRUCT;
	MC_Config_typ : 	STRUCT 
		DataObjectName : STRING[8];
		UnitBasis : UINT;
		UnitFactor : UDINT;
		AxisPeriod : UDINT;
	END_STRUCT;
	MC_Data_AxisLimits_typ : 	STRUCT 
		AXLIM_V : REAL;
		AXLIM_A : REAL;
		AXLIM_POS_SW_END : DINT;
		AXLIM_NEG_SW_END : DINT;
		AXLIM_DS_STOP : REAL;
	END_STRUCT;
	MC_Data_typ : 	STRUCT 
		DriveStatus : MC_DRIVESTATUS_TYP;
		UDCNominal : REAL;
	END_STRUCT;
	MC_FB_typ : 	STRUCT 
		MC_Power : MC_Power;
		MC_Home : MC_Home;
		TON_HomingTimeout : TON;
		TON_AxisErrorAcknowledgeDelay : TON;
		MC_MoveAbsolute : MC_MoveAbsolute;
		MC_MoveAdditive : MC_MoveAdditive;
		MC_MoveVelocity : MC_MoveVelocity;
		MC_GearIn : MC_GearIn;
		MC_CamIn : MC_CamIn;
		MC_BR_InitAutData : MC_BR_InitAutData;
		MC_BR_AutCommand : MC_BR_AutCommand;
		MC_BR_VelocityControl : MC_BR_VelocityControl;
		MC_Stop : MC_Stop;
		MC_Halt : MC_Halt;
		MC_BR_ReadDriveStatus : MC_BR_ReadDriveStatus;
		MC_ReadStatus : MC_ReadStatus;
		MC_ReadAxisError : MC_ReadAxisError;
		MC_Reset : MC_Reset;
		MC_ReadActualPosition : MC_ReadActualPosition;
		MC_ReadActualVelocity : MC_ReadActualVelocity;
		MC_BR_Simulation : MC_BR_Simulation;
		MC_BR_InitCyclicRead : MC_BR_InitCyclicRead;
		MC_BR_InitCyclicWrite : MC_BR_InitCyclicWrite;
		MC_BR_ReadParID : MC_BR_ReadParID;
		MC_BR_WriteParID : MC_BR_WriteParID;
		MC_BR_InitParTabObj : MC_BR_InitParTabObj;
		MC_BR_InitParList : MC_BR_InitParList;
		MC_BR_InitAxisPar : MC_BR_InitAxisPar;
		MC_BR_InitAxisSubjectPar : MC_BR_InitAxisSubjectPar;
		MC_BR_SetupController : MC_BR_SetupController;
		MC_BR_InitModPos : MC_BR_InitModPos;
		MC_BR_DownloadCamProfileData : MC_BR_DownloadCamProfileData;
		MC_BR_DownloadCamProfileObj : MC_BR_DownloadCamProfileObj;
		MC_BR_ReadAutPosition : MC_BR_ReadAutPosition;
	END_STRUCT;
	MC_typ : 	STRUCT 
		State : UINT;
		LastState : UINT;
		StateTrace : ARRAY[0..99] OF UINT;
		EState : UINT;
		ReturnState : UINT;
		IStatus : INT;
		CStatus : INT;
		NCStatus : UINT;
		AXIS_adr : UDINT;
		VAXIS_adr : UDINT;
		FB : MC_FB_typ;
		data : MC_Data_typ;
		config : MC_Config_typ;
	END_STRUCT;
	PLS_typ : 	STRUCT 
		TON : REAL;
		TOFF : REAL;
		TimerPT : REAL;
		Hysteresis : REAL;
	END_STRUCT;
	Run_typ : 	STRUCT 
		ResetRunLengthPB : BOOL;
		JobInProgress : BOOL;
		RunComplete : BOOL;
		MoveMode : USINT;
		LengthToRun : LREAL;
		LengthRun : LREAL;
		Ratio : REAL; (*table revs / in*)
		TargetPosition : ARRAY[0..7] OF LREAL;
		CommandPosition : ARRAY[0..7] OF REAL;
		AxisTruePositionI : ARRAY[0..7] OF LREAL;
		AxisTruePosition : ARRAY[0..7] OF LREAL;
		AxisPosition_last : ARRAY[0..7] OF REAL;
		AxisOffset : ARRAY[0..7] OF LREAL;
		LineDir : BOOL; (*1=reverse*)
		PayOffTraverseDir : BOOL; (*when the line is running forward, 0=forward, 1=reverse. when the line is running reverse, 0=reverse, 1=forward.*)
		TakeUpTraverseDir : BOOL; (*when the line is running forward, 0=forward, 1=reverse. when the line is running reverse, 0=reverse, 1=forward.*)
		PayOffSpoolOD : REAL;
		StripPayOff1OD : REAL;
		StripPayOff2OD : REAL;
		TakeUpSpoolOD : REAL;
		TakeUpSpoolRevCount : LREAL;
	END_STRUCT;
	Recipe_typ : 	STRUCT 
		Name : STRING[20];
		RunMode : BOOL; (*0 = Continuous, 1 = Discrete*)
		Distance : REAL; (* feet*)
		Braider : Recipe_Braider_typ;
		Solder : Recipe_Solder_typ;
		Puller : Recipe_Puller_typ;
		PayOff : Recipe_PayOff_typ;
		StripPayOff : ARRAY[0..1] OF Recipe_StripPayOff_typ;
		TakeUp : Recipe_TakeUp_typ;
	END_STRUCT;
	Recipe_Solder_typ : 	STRUCT 
		IgnoreSolderStation : BOOL; (*if set to one, then the solder station interface is ignored*)
		StopDelay : USINT; (*Seconds to keep running after a stop button or critical fault when controling solder station*)
	END_STRUCT;
	Recipe_Braider_typ : 	STRUCT 
		Disabled : BOOL;
		PPI : REAL;
		CarrierMode : BOOL; (*0 = Full, 1 = Half*)
		PullerSpeed : REAL;
		WireBreakDist : REAL; (*inches*)
	END_STRUCT;
	Recipe_Puller_typ : 	STRUCT 
		PullerSpeed : REAL;
	END_STRUCT;
	Recipe_StripPayOff_typ : 	STRUCT 
		Disabled : BOOL;
		SpoolID : REAL; (*OD goes in active memory!*)
		Tension : REAL;
		TensionMin : REAL;
		TensionMax : REAL;
	END_STRUCT;
	Recipe_TakeUp_typ : 	STRUCT 
		SpoolMode : UINT; (*0=normal, 1=standalone, 2=disabled*)
		SpoolID : REAL; (*OD goes in active memory!*)
		SpoolSpeed : REAL;
		Tension : REAL;
		TensionMin : REAL;
		TensionMax : REAL;
		TraverseDisabled : BOOL;
		TraverseMode : BOOL; (*0 = pitch only, 1 = centerline displacement correction*)
		TraversePitch : REAL;
		TraverseTurnAroundDistance : REAL;
		TraverseMaxCenterlineDeviation : REAL;
		TraverseCorrectionFrequency : REAL;
		TraversePauseAtFlange : BOOL;
	END_STRUCT;
	Recipe_PayOff_typ : 	STRUCT 
		SpoolDisabled : BOOL;
		SpoolID : REAL; (*OD goes in active memory!*)
		Tension : REAL;
		TensionMin : REAL;
		TensionMax : REAL;
		TraverseDisabled : BOOL;
		TraverseMode : BOOL; (*0 = pitch only, 1 = centerline displacement correction*)
		TraversePitch : REAL;
		TraverseTurnAroundDistance : REAL;
		TraverseMaxCenterlineDeviation : REAL;
		TraverseCorrectionFrequency : REAL;
	END_STRUCT;
	SystemSettings_TuningPar_typ : 	STRUCT 
		POS_CTRL_KV : REAL;
		SCTRL_KV : REAL;
		SCTRL_TN : REAL;
		SCTRL_T_FILTER : REAL;
		FF_TORQUE_LOAD : REAL; (* Load torque *)
		FF_TORQUE_POS : REAL; (* Torque in positive direction *)
		FF_TORQUE_NEG : REAL; (* Torque in negative direction *)
		FF_KV_TORQUE : REAL; (* Speed torque factor *)
		FF_INERTIA : REAL; (* Mass moment of inertia *)
	END_STRUCT;
	SystemSettings_Limits_typ : 	STRUCT 
		AXLIM_V : REAL;
		AXLIM_A : REAL;
		AXLIM_POS_SW_END : REAL;
		AXLIM_NEG_SW_END : REAL;
		AXLIM_DS_STOP : REAL;
	END_STRUCT;
	SystemSettings_AxisConfig_typ : 	STRUCT 
		Accel : REAL;
		Decel : REAL;
		JogSpeed : REAL;
		EncoderDirection : UINT;
		EncoderValidation : UINT;
		ScalingEncoderCounts : UDINT;
		ScalingMotorRevs : UDINT;
		TemperatureModelDisable : UINT;
		Tuning : SystemSettings_TuningPar_typ;
		Limits : SystemSettings_Limits_typ;
		Spares : ARRAY[0..19] OF REAL;
	END_STRUCT;
	SystemSettings_User_typ : 	STRUCT 
		User : STRING[15];
		Password : STRING[6];
	END_STRUCT;
	SystemSettings_LoadCell_typ : 	STRUCT 
		TareForce : ARRAY[0..2] OF REAL;
		TareValue : ARRAY[0..2] OF REAL;
		ForceLimit : REAL;
	END_STRUCT;
	SystemSettings_TensionCtrl_typ : 	STRUCT 
		Kp : REAL; (*Proportional Gain*)
		Tn : REAL; (*Integral Gain*)
		Tv : REAL; (*Derivative Gain*)
		Tf : REAL; (*Derivative Filter*)
	END_STRUCT;
	SystemSettings_FootPedal_typ : 	STRUCT 
		MinVal : INT;
		MaxVal : INT;
	END_STRUCT;
	SystemSettings_CenterLine_typ : 	STRUCT 
		ExtentPositive : INT;
		ExtentNegative : INT;
		Range : REAL;
	END_STRUCT;
	SystemSettings_typ : 	STRUCT 
		RecipeNum : UINT;
		MaxRecipes : UINT;
		ModelNumber : STRING[25];
		SerialNumber : STRING[25];
		CustomerField : STRING[25];
		UnitType : UINT;
		Language : UINT;
		LineVoltage : UINT;
		PhaseMonIgnore : UINT;
		StartWarningTime : REAL;
		MinPPI : REAL;
		MaxPPI : REAL;
		Reserved : ARRAY[0..2] OF STRING[6];
		HMI2IPAddress : ARRAY[0..3] OF USINT;
		LoadCell : ARRAY[0..3] OF SystemSettings_LoadCell_typ;
		TensionCtrl : ARRAY[0..3] OF SystemSettings_TensionCtrl_typ;
		CenterLine : ARRAY[0..1] OF SystemSettings_CenterLine_typ;
		AxisConfig : ARRAY[0..7] OF SystemSettings_AxisConfig_typ;
		FootPedal : SystemSettings_FootPedal_typ;
	END_STRUCT;
END_TYPE
