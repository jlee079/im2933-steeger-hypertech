
FUNCTION_BLOCK MovingAverageN
	VAR_INPUT
		Enable : BOOL;
		Reset : BOOL;
		ReplaceLastInputData : BOOL;
		InputData : REAL;
		N : UINT;
	END_VAR
	VAR_OUTPUT
		MovingAverage : REAL;
		BufferFilled : BOOL;
	END_VAR
	VAR
		N_max : UINT;
		Data : ARRAY[0..2499] OF REAL;
		DataStart : UINT;
		DataNext : UINT;
		DataCount : UINT;
		SamplesToAverage : UINT;
		sum : LREAL;
		i : DINT;
		zzEdge00000 : BOOL;
		EDGEPOSReset : BOOL;
	END_VAR
END_FUNCTION_BLOCK
