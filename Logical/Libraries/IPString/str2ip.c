#include "IPString.h"
#include <AsString.h>
#include <string.h>

void Str2IP(Str2IP_typ* inst) 
{
	char *dot;
	if (inst->IPAddress != 0) {
		dot = strtok((char*)inst->IPAddress, ".");
		inst->Octet1 = atoi((unsigned long)dot);
		dot = strtok(0, ".");
		inst->Octet2 = atoi((unsigned long)dot);
		dot = strtok(0, ".");
		inst->Octet3 = atoi((unsigned long)dot);
		dot = strtok(0, ".");
		inst->Octet4 = atoi((unsigned long)dot);		
	}

}
