(********************************************************************
 * COPYRIGHT -- Integrated Motion, Inc.
 ********************************************************************
 * Library: VCHelper
 * File: VCHelper.fun
 * Author: sswindells
 * Created: January 15, 2009
 ********************************************************************
 * Functions and function blocks of library VCHelper
 ********************************************************************)

FUNCTION HIDE : BOOL
	VAR_INPUT
		SDP : UDINT;
	END_VAR
	VAR
		temp : UINT;
	END_VAR
END_FUNCTION

FUNCTION SHOW : USINT
	VAR_INPUT
		SDP : UDINT;
	END_VAR
	VAR
		temp : UINT;
	END_VAR
END_FUNCTION

FUNCTION UNLOCK : USINT
	VAR_INPUT
		SDP : UDINT;
	END_VAR
	VAR
		temp : UINT;
	END_VAR
END_FUNCTION

FUNCTION LOCK : USINT
	VAR_INPUT
		SDP : UDINT;
	END_VAR
	VAR
		temp : UINT;
	END_VAR
END_FUNCTION
