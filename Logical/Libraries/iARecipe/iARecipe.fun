(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: IMRecipe
 * File: IMRecipe.fun
 * Author: sswindells
 * Created: October 11, 2010
 ********************************************************************
 * Functions and function blocks of library IMRecipe
 ********************************************************************)

FUNCTION_BLOCK DataManagerCyclic
	VAR_INPUT
		cmd : UINT;
		NRec : UINT;
		NRecDst : UINT;
		Name : STRING[20];
		Internal : REFERENCE TO DataManager_Internal_typ;
		RecipeMismatch : REFERENCE TO BOOL;
		ActiveRecipe : REFERENCE TO UINT;
		MaxRecipes : REFERENCE TO UINT;
		NewSys : REFERENCE TO BOOL;
	END_VAR
	VAR_OUTPUT
		Init : BOOL;
		Status : UINT; (*Status Information (Ready, Not Ready, Error)*)
		ErrorID : UINT; (*Primary Error Information*)
		ErrorID2 : UINT; (*Extended Error Information*)
		RecipeNames : ARRAY[0..999] OF STRING[20]; (*List of Raw Recipe Names*)
		RecipeNames_vc : ARRAY[0..999] OF STRING[25]; (*List of Recipe Names with '##.) ' in front*)
	END_VAR
	VAR CONSTANT
		MAXRECIPES : UINT := 1000; (*NOTE: IF YOU NEED MORE RECIPES MAKE SURE TO ADJUST THE ABOVE ARRAY SIZES TOO*)
	END_VAR
	VAR
		Step : UINT;
		LastStep : UINT;
		StepTrace : ARRAY[0..99] OF UINT;
		i : UINT;
		numstring : STRING[5];
	END_VAR
	VAR CONSTANT
		STATE_IDLE : UINT := 0;
		STATE_INIT_SYS_INFO : UINT := 2;
		STATE_INIT_SYS_LOAD : UINT := 3;
		STATE_INIT_SYS_CREATE : UINT := 4;
		STATE_INIT_REC_INFO : UINT := 5;
		STATE_INIT_REC_INFO2 : UINT := 6;
		STATE_INIT_REC_READNAME : UINT := 7;
		STATE_INIT_REC_LOAD : UINT := 8;
		STATE_INIT_REC_LOAD2 : UINT := 9;
		STATE_RECIPE_LOAD : UINT := 10;
		STATE_RECIPE_CREATE : UINT := 15;
		STATE_RECIPE_SAVE : UINT := 20;
		STATE_RECIPE_COPY_LOAD : UINT := 30;
		STATE_RECIPE_COPY_CREATE : UINT := 31;
		STATE_RECIPE_COPY_SAVE : UINT := 32;
		STATE_RECIPE_RENAME_LOAD : UINT := 40;
		STATE_RECIPE_RENAME_CREATE : UINT := 41;
		STATE_RECIPE_RENAME_SAVE : UINT := 42;
		STATE_RECIPE_VIEW : UINT := 50;
		STATE_RECIPE_DELETE : UINT := 60;
		STATE_RECIPE_DELETE_ALL : UINT := 70;
		STATE_RECIPE_DELETE_ALL_WAIT : UINT := 71;
		STATE_RECIPE_IMPORT : UINT := 80;
		STATE_RECIPE_IMPORT_CREATE : UINT := 81;
		STATE_RECIPE_IMPORT_SAVE : UINT := 82;
		STATE_RECIPE_IMPORT_ALL : UINT := 90;
		STATE_RECIPE_IMPORT_ALL_CREATE : UINT := 91;
		STATE_RECIPE_IMPORT_ALL_SAVE : UINT := 92;
		STATE_RECIPE_EXPORT_LOAD : UINT := 100;
		STATE_RECIPE_EXPORT_SEARCH : UINT := 101;
		STATE_RECIPE_EXPORT_WRITE : UINT := 102;
		STATE_RECIPE_EXPORT_ALL : UINT := 110;
		STATE_RECIPE_EXPORT_ALL_LOAD : UINT := 111;
		STATE_RECIPE_EXPORT_ALL_SEARCH : UINT := 112;
		STATE_RECIPE_EXPORT_ALL_WRITE : UINT := 113;
		STATE_RECIPE_BCKGND_LOAD : UINT := 120;
		STATE_RECIPE_BCKGND_CREATE : UINT := 130;
		STATE_RECIPE_BCKGND_SAVE : UINT := 131;
		STATE_SYS_SAVE : UINT := 160;
		STATE_SYS_IMPORT : UINT := 170;
		STATE_SYS_IMPORT_SAVE : UINT := 171;
		STATE_SYS_EXPORT : UINT := 180;
		STATE_SYS_EXPORT2 : UINT := 181;
		STATE_DONE : UINT := 400;
		STATE_INTERNAL_ERROR : UINT := 500;
	END_VAR
	VAR
		TON_AutoSaveInterval : TON;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK DataManagerInit
	VAR_INPUT
		Config : REFERENCE TO DataManager_Config_typ;
		Internal : REFERENCE TO DataManager_Internal_typ;
	END_VAR
	VAR_OUTPUT
		Status : UINT; (*Status Information (Ready, Not Ready, Error)*)
	END_VAR
	VAR
		MemPartCreate : AsMemPartCreate;
		MemPartAlloc : AsMemPartAlloc;
	END_VAR
END_FUNCTION_BLOCK
