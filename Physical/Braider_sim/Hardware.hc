 
[hardwarekonfig]
wurzel: 3
[2.1]
[2.100]
moduladr: 0
version: 1.0.0.0
steckplatzgrp: "cpubus"
steckplatzinfo: NUMMER=1, MODULID=1, FLAGS=1
{2.101}
klasse: "BPAR000"
[3.1]
spi: OBJEKTID=4
{3.3}
klasse: "HC_Moduluebersicht.Class"
name: _PRJ-Moduluebersicht
[8.1]
[8.100]
moduladr: 0
verbunden: 5
knotennr: 3
station: 3
busart: 9
modulname: TakeUpSpool
version: 1.0.0.0
[8.101]
[8.107]
[8.1456]
ncsoftware: NAME="acp10", ID=128, NETWORK=0x8081
{8.1824}
klasse: "SimACOPOS"
[9.1]
[9.100]
moduladr: 0
verbunden: 5
knotennr: 4
station: 4
busart: 9
modulname: TakeUpTraverse
version: 1.0.0.0
[9.101]
[9.107]
[9.1456]
ncsoftware: NAME="acp10", ID=128, NETWORK=0x8081
{9.1824}
klasse: "SimACOPOS"
[10.1]
[10.100]
moduladr: 0
verbunden: 5
knotennr: 5
station: 5
busart: 9
modulname: Puller
version: 1.0.0.0
[10.101]
[10.107]
[10.1456]
ncsoftware: NAME="acp10", ID=128, NETWORK=0x8081
{10.1824}
klasse: "SimACOPOS"
[6.1]
[6.100]
moduladr: 0
verbunden: 5
knotennr: 1
station: 1
busart: 9
modulname: PayOffSpool
version: 1.0.0.0
[6.101]
[6.107]
[6.1456]
ncsoftware: NAME="acp10", ID=128, NETWORK=0x8081
{6.1824}
klasse: "SimACOPOS"
[7.1]
[7.100]
moduladr: 0
verbunden: 5
knotennr: 2
station: 2
busart: 9
modulname: PayOffTraverse
version: 1.0.0.0
[7.101]
[7.107]
[7.1456]
ncsoftware: NAME="acp10", ID=128, NETWORK=0x8081
{7.1824}
klasse: "SimACOPOS"
[14.1]
[14.100]
moduladr: 0
verbunden: 5
knotennr: 6
station: 6
busart: 9
modulname: Table
version: 1.0.0.0
[14.101]
[14.107]
[14.1456]
ncsoftware: NAME="acp10", ID=128, NETWORK=0x8081
{14.1824}
klasse: "SimACOPOS"
[1.1]
[1.100]
moduladr: 1
modulname: CPU
version: 1.0.3.0
steckplatzgrp: "ar000d"
steckplatzinfo: NUMMER=1, MODULID=0, FLAGS=0
steckplatzgrp: "simmotionbus"
steckplatzinfo: NUMMER=1, MODULID=5, FLAGS=1
anschlussgrp: "ser1"
anschlussinfo: NUMMER=1, MODULID=0, FLAGS=0
anschlussgrp: "ser2"
anschlussinfo: NUMMER=1, MODULID=0, FLAGS=0
anschlussgrp: "eth3"
anschlussinfo: NUMMER=1, MODULID=0, FLAGS=0
anschlussgrp: "vir7"
anschlussinfo: NUMMER=1, MODULID=13, FLAGS=1
anschlussinfo: NUMMER=2, MODULID=17, FLAGS=1
[1.101]
[1.109]
[1.1632]
[1.1232]
[1.1824]
{1.108}
klasse: "1A4000.00"
[4.1]
spi: OBJEKTID=2
spi: OBJEKTID=13
spi: OBJEKTID=17
spi: OBJEKTID=6
spi: OBJEKTID=7
spi: OBJEKTID=8
spi: OBJEKTID=9
spi: OBJEKTID=10
spi: OBJEKTID=14
{4.2}
klasse: "HC_SPS.Class"
familie: 43
modulname: PLC1
[5.1]
[5.100]
moduladr: 1
slotnr: 1
version: 1.0.0.1
anschlussgrp: "vir1"
anschlussinfo: NUMMER=1, MODULID=6, FLAGS=1
anschlussinfo: NUMMER=2, MODULID=7, FLAGS=1
anschlussinfo: NUMMER=3, MODULID=8, FLAGS=1
anschlussinfo: NUMMER=4, MODULID=9, FLAGS=1
anschlussinfo: NUMMER=5, MODULID=10, FLAGS=1
anschlussinfo: NUMMER=6, MODULID=14, FLAGS=1
[5.101]
{5.1232}
klasse: "MotionIF"
[13.1]
[13.100]
moduladr: 0
verbunden: 1
knotennr: 1
station: 1
busart: 9
version: 1.0.0.3
steckplatzgrp: "ps2bus"
steckplatzinfo: NUMMER=1, MODULID=0, FLAGS=0
[13.101]
[13.1248]
{13.1824}
klasse: "VNCQVGA"
[17.1]
[17.100]
moduladr: 0
verbunden: 1
knotennr: 2
station: 2
busart: 9
version: 1.0.0.3
steckplatzgrp: "ps2bus"
steckplatzinfo: NUMMER=1, MODULID=0, FLAGS=0
[17.101]
[17.1248]
{17.1824}
klasse: "VNCQVGA"
