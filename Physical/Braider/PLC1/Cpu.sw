﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=3.0.81.32 SP07?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="hwconfig" Source="Programs.hwconfig_braider.prg" Memory="UserROM" Language="IEC" />
    <Task Name="loadcells" Source="Programs.loadcells.prg" Memory="UserROM" Language="IEC" />
    <Task Name="centerline" Source="Programs.centerline.prg" Memory="UserROM" Language="IEC" />
    <Task Name="footpedal" Source="Programs.footpedal.prg" Memory="UserROM" Language="IEC" />
    <Task Name="mm_puller" Source="Programs.mm_puller.prg" Memory="UserROM" Language="IEC" />
    <Task Name="mm_braider" Source="Programs.mm_braider.prg" Memory="UserROM" Language="IEC" />
    <Task Name="mm_payoff" Source="Programs.mm_payoff.prg" Memory="UserROM" Language="IEC" />
    <Task Name="mm_takeup" Source="Programs.mm_takeup.prg" Memory="UserROM" Language="IEC" />
    <Task Name="axis" Source="Programs.axis.prg" Memory="UserROM" Language="IEC" />
    <Task Name="alarmmgr" Source="Programs.alarmmgr.prg" Memory="UserROM" Language="IEC" />
    <Task Name="DataMonito" Source="Programs.DataCollection.DataMonitor.prg" Memory="UserROM" Language="IEC" />
  </TaskClass>
  <TaskClass Name="Cyclic#2">
    <Task Name="hmimgr" Source="Programs.hmimgr.prg" Memory="UserROM" Language="IEC" />
    <Task Name="ioforce" Source="Programs.ioforce_braider.prg" Memory="UserROM" Language="IEC" />
    <Task Name="buildinfo" Source="Programs.buildinfo.prg" Memory="UserROM" Language="IEC" />
  </TaskClass>
  <TaskClass Name="Cyclic#3">
    <Task Name="alarmmsg" Source="Programs.alarmmsg.prg" Memory="UserROM" Language="IEC" />
    <Task Name="datamgr" Source="Programs.datamgr.prg" Memory="UserROM" Language="IEC" />
    <Task Name="DataExport" Source="Programs.DataCollection.DataExport.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <DataObjects>
    <DataObject Name="arsdmsvp" Source="" Memory="UserROM" Language="Binary" />
    <DataObject Name="hmi2cfg" Source="" Memory="UserROM" Language="Binary" TransferOnlyIfNotOnTarget="true" />
    <DataObject Name="arsdmsvg" Source="" Memory="UserROM" Language="Binary" />
    <DataObject Name="arsdmhtm" Source="" Memory="UserROM" Language="Binary" />
    <DataObject Name="Acp10sys" Source="" Memory="UserROM" Language="Binary" />
  </DataObjects>
  <NcDataObjects>
    <NcDataObject Name="m_8jsa22" Source="MotionObjects.m_8jsa22.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="m_8jsa51" Source="MotionObjects.m_8jsa51.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="err_txt" Source="MotionObjects.err_txt.dob" Memory="UserROM" Language="Ett" />
    <NcDataObject Name="x_pos" Source="MotionObjects.x_pos.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="x_pot" Source="MotionObjects.x_pot.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="x_tus" Source="MotionObjects.x_tus.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="x_tut" Source="MotionObjects.x_tut.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="x_puller" Source="MotionObjects.x_puller.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="x_table" Source="MotionObjects.x_table.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="x_strpo1" Source="MotionObjects.x_strpo1.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="x_strpo2" Source="MotionObjects.x_strpo2.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="acp10sys" Source="" Memory="UserROM" Language="Binary" />
  </NcDataObjects>
  <VcDataObjects>
    <VcDataObject Name="hmi" Source="Visualization.hmi.dob" Memory="UserROM" Language="Vc" WarningLevel="2" Compress="false" />
    <VcDataObject Name="hmi2" Source="Visualization.hmi2.dob" Memory="UserROM" Language="Vc" WarningLevel="2" Compress="false" />
  </VcDataObjects>
  <Binaries>
    <BinaryObject Name="vcfntttf" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbmp" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcshared" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdvnc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcresman" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccshape" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="visvc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcgclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfile" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcchspot" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcmgr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdpp30" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="hmi202" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="hmi201" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbtn" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsloc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccovl" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccpopup" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcctext" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcbclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbar" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpfmtcx" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpfpp30" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcrt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccnum" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccstr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccddbox" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcctrend" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdsw" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcvisapi" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcnet" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="webserv" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="tahomabd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arsdm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="tahoma" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="hmi02" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="hmi03" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="hmi01" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsint" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vctcal" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccdt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="hmi203" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcptelo" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcclbox" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Acp10cfg" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Acp10map" Source="" Memory="UserROM" Language="Binary" />
  </Binaries>
  <Libraries>
    <LibraryObject Name="IPString" Source="Libraries.IPString.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="fputil" Source="Libraries.fputil.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.sys_lib.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="brsystem" Source="Libraries.brsystem.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="runtime" Source="Libraries.runtime.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="AsARCfg" Source="Libraries.AsARCfg.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="asstring" Source="Libraries.asstring.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="DataObj" Source="Libraries.DataObj.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="FileIO" Source="Libraries.FileIO.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="astime" Source="Libraries.astime.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="Convert" Source="Libraries.Convert.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="standard" Source="Libraries.standard.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="AsMath" Source="Libraries.AsMath.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="AsIO" Source="Libraries.AsIO.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="VCHelper" Source="Libraries.VCHelper.lby" Memory="UserROM" Language="IEC" />
    <LibraryObject Name="NcGlobal" Source="Libraries.NcGlobal.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="Acp10par" Source="Libraries.Acp10par.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="Acp10man" Source="Libraries.Acp10man.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="Acp10_MC" Source="Libraries.Acp10_MC.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="Acp10sdc" Source="Libraries.Acp10sdc.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="fputil2" Source="Libraries.fputil2.lby" Memory="UserROM" Language="IEC" />
    <LibraryObject Name="Visapi" Source="Libraries.Visapi.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="AsMem" Source="Libraries.AsMem.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="iARecipe" Source="Libraries.iARecipe.lby" Memory="UserROM" Language="IEC" />
    <LibraryObject Name="MovingAvg" Source="Libraries.MovingAvg.lby" Memory="UserROM" Language="IEC" />
    <LibraryObject Name="AsHW" Source="Libraries.AsHW.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="LoopConR" Source="Libraries.LoopConR.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="operator" Source="Libraries.operator.lby" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="LREAL_Tool" Source="Programs.DataCollection.LREAL_Tool.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="CSV_File" Source="Libraries.CSV_File.lby" Memory="UserROM" Language="IEC" />
    <LibraryObject Name="powerlnk" Source="" Memory="UserROM" Language="Binary" />
    <LibraryObject Name="astcp" Source="" Memory="UserROM" Language="Binary" />
  </Libraries>
</SwConfiguration>